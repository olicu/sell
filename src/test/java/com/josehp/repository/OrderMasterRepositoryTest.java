package com.josehp.repository;

import com.josehp.dataobject.OrderMaster;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * @ClassName: OrderMasterRepositoryTest
 * @Package com.josehp.repository
 * @Description:
 * @Author Josehp
 * @CreateDate 2018/02/15 15:45
 * @Version V1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderMasterRepositoryTest {
	
	@Autowired
	private OrderMasterRepository repository;
	
	private final String OPENID = "110110";
	
	@Test
	public void saveTest() {
		OrderMaster orderMaster = new OrderMaster();
		orderMaster.setOrderId("1234567");
		orderMaster.setBuyerName("zxc");
		orderMaster.setBuyerPhone("2345678901");
		orderMaster.setBuyerAddress("asd");
		orderMaster.setBuyerOpenid(OPENID);
		orderMaster.setOrderAmount(new BigDecimal(2.5));
		OrderMaster save = repository.save(orderMaster);
		Assert.assertNotNull(save);
	}
	
	@Test
	public void findByBuyerOpenid() throws Exception {
		PageRequest request = new PageRequest(0, 3);
		Page <OrderMaster> result = repository.findByBuyerOpenid(OPENID, request);
		//	System.out.println(result.getTotalElements());
		Assert.assertNotEquals(0, result.getTotalElements());
	}
	
}