package com.josehp.repository;

import com.josehp.dataobject.OrderDetail;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @ClassName: OrderDetailRepositoryTest
 * @Package com.josehp.repository
 * @Description:
 * @Author Josehp
 * @CreateDate 2018/02/15 16:02
 * @Version V1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderDetailRepositoryTest {
	@Autowired
	private OrderDetailRepository repository;
	
	@Test
	public void saveTest() {
		OrderDetail orderDetail = new OrderDetail();
		orderDetail.setDetailId("123456789");
		orderDetail.setOrderId("1234567");
		orderDetail.setProductIcon("http:111.jpg");
		orderDetail.setProductId("1111112");
		orderDetail.setProductName("批单子");
		orderDetail.setProductPrice(new BigDecimal(1.2));
		orderDetail.setProductQuantity(2);
		
		OrderDetail save = repository.save(orderDetail);
		Assert.assertNotNull(save);
	}
	
	@Test
	public void findByOrderID() throws Exception {
		List <OrderDetail> byOrderId = repository.findByOrderId("123456");
		Assert.assertNotEquals(0, byOrderId.size());
	}
	
}