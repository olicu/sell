package com.josehp.repository;

import com.josehp.dataobject.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;


import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @ClassName: ProductCategoryRepositoryTest
 * @Package com.josehp.repository
 * @Description: 类目Dao测试
 * @Author Josehp
 * @CreateDate 2018/02/13 16:25
 * @Version V1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductCategoryRepositoryTest {
	@Autowired
	private ProductCategoryRepository repository;
	
	@Test
	public void findOneTest() {
		ProductCategory productCategory = repository.findOne(1);
		System.out.println(productCategory.toString());
	}
	
	@Test
	@Transactional
	public void saveTest() {
		ProductCategory productCategory = new ProductCategory("测试7", 7);
		ProductCategory result = repository.save(productCategory);
		Assert.assertNotNull(result);
		//	Assert.assertNotEquals(null,result);
		
	}
	
	@Test
	public void findByCategoryTypeInTest() {
		List <Integer> list = Arrays.asList(4, 6, 7);
		List <ProductCategory> result = repository.findByCategoryTypeIn(list);
		Assert.assertNotEquals(0, result.size());
	}
}