package com.josehp.repository;

import com.josehp.dataobject.ProductInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @ClassName: ProductInfoRepositoryTest
 * @Package com.josehp.repository
 * @Description: 商品Dao测试
 * @Author Josehp
 * @CreateDate 2018/02/13 16:12
 * @Version V1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductInfoRepositoryTest {
	@Autowired
	private ProductInfoRepository repository;
	
	@Test
	public void save() throws Exception {
		ProductInfo productInfo = new ProductInfo();
		productInfo.setProductId("123456");
		productInfo.setProductName("皮蛋粥");
		productInfo.setProductPrice(new BigDecimal(3.2));
		productInfo.setProductStock(100);
		productInfo.setProductIcon("http://xxx.jpg");
		productInfo.setProductDescription("很好喝的粥");
		productInfo.setProductStatus(0);
		productInfo.setCategoryType(6);
		
		
		ProductInfo result = repository.save(productInfo);
		Assert.assertNotNull(result);
		
	}
	
	@Test
	public void findByProductStatus() {
		List <ProductInfo> productInfoList = repository.findByProductStatus(0);
		Assert.assertNotEquals(0, productInfoList.size());
		
	}
	
}