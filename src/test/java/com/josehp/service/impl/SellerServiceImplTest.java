package com.josehp.service.impl;

import com.josehp.dataobject.SellerInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.swing.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class SellerServiceImplTest {
	public static final String openid = "abc";
	@Autowired
	private SellerServiceImpl sellerService;
	
	@Test
	public void findSellerInfoByOpenid() throws Exception {
		SellerInfo sellerInfo = sellerService.findSellerInfoByOpenid(openid);
		Assert.assertEquals(openid, sellerInfo.getOpenid());
	}
	
}