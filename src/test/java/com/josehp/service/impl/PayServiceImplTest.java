package com.josehp.service.impl;

import com.josehp.dto.OrderDTO;
import com.josehp.service.OrderService;
import com.josehp.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@Slf4j
@SpringBootTest
public class PayServiceImplTest {
	@Autowired
	private PayService payService;
	@Autowired
	private OrderService orderService;
	
	@Test
	public void create() throws Exception {
		OrderDTO orderDTO = orderService.findOne("1519114031460520428");
		payService.create(orderDTO);
	}
	
	@Test
	public void refund() {
		OrderDTO orderDTO = orderService.findOne("1519114031460520428");
		payService.refund(orderDTO);
	}
	
}