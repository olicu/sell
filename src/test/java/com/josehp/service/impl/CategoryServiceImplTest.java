package com.josehp.service.impl;

import com.josehp.dataobject.ProductCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceImplTest {
	@Autowired
	private CategoryServiceImpl categoryService;
	
	@Test
	public void findOne() throws Exception {
		ProductCategory one = categoryService.findOne(1);
		Assert.assertEquals(new Integer(1), one.getCategoryId());
	}
	
	@Test
	public void findAll() throws Exception {
		List <ProductCategory> all = categoryService.findAll();
		Assert.assertNotEquals(0, all.size());
	}
	
	@Test
	public void findByCategoryTypeIn() throws Exception {
		List <ProductCategory> byCategoryTypeIn = categoryService.findByCategoryTypeIn(Arrays.asList(1, 2, 3, 4));
		Assert.assertNotEquals(0, byCategoryTypeIn.size());
	}
	
	@Test
	public void save() throws Exception {
		ProductCategory productCategory = new ProductCategory("测试222", 22);
		ProductCategory result = categoryService.save(productCategory);
		Assert.assertNotNull(result);
	}
	
}