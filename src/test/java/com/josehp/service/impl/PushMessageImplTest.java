package com.josehp.service.impl;

import com.josehp.dto.OrderDTO;
import com.josehp.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class PushMessageImplTest {
	@Autowired
	private PushMessageServiceImpl pushMessageService;
	@Autowired
	private OrderService orderService;
	
	@Test
	public void orderStatus() throws Exception {
		OrderDTO orderDTO = orderService.findOne("1520064571074621141");
		pushMessageService.orderStatus(orderDTO);
	}
	
}