package com.josehp.service.impl;

import com.josehp.dataobject.OrderDetail;
import com.josehp.dto.OrderDTO;
import com.josehp.enums.OrderStatusEnum;
import com.josehp.enums.PayStatusEnum;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OrderServiceImplTest {
	@Autowired
	private OrderServiceImpl orderService;
	
	
	private final String BUYER_OPENID = "1101110";
	
	private final String ORDER_ID = "1519029018373584530";
	
	@Test
	public void create() throws Exception {
		OrderDTO orderDTO = new OrderDTO();
		orderDTO.setBuyerName("josehp");
		orderDTO.setBuyerAddress("zzzzz");
		orderDTO.setBuyerPhone("12345678901");
		orderDTO.setBuyerOpenid(BUYER_OPENID);
		//购物车
		List <OrderDetail> orderDetailList = new ArrayList <>();
		
		OrderDetail o1 = new OrderDetail();
		o1.setProductId("12345678");
		o1.setProductQuantity(1);
		
		
		OrderDetail o2 = new OrderDetail();
		o2.setProductId("1234567");
		o2.setProductQuantity(1);
		orderDetailList.add(o1);
		orderDetailList.add(o2);
		orderDTO.setOrderDetailList(orderDetailList);
		
		OrderDTO result = orderService.create(orderDTO);
		log.info("【创建订单】 result={}", result);
	}
	
	@Test
	public void findOne() throws Exception {
		OrderDTO orderDTO = orderService.findOne(ORDER_ID);
		log.info("【查询单个订单】result={}", orderDTO);
		Assert.assertEquals(ORDER_ID, orderDTO.getOrderId());
	}
	
	@Test
	public void findList() throws Exception {
		PageRequest request = new PageRequest(0, 2);
		Page <OrderDTO> orderDTOPage = orderService.findList("1101110", request);
		Assert.assertNotEquals(0, orderDTOPage.getTotalElements());
		
	}
	
	@Test
	public void cancel() throws Exception {
		OrderDTO orderDTO = orderService.findOne(ORDER_ID);
		OrderDTO result = orderService.cancel(orderDTO);
		Assert.assertEquals(OrderStatusEnum.CANCEL.getCode(), result.getOrderStatus());
	}
	
	@Test
	public void finish() throws Exception {
		OrderDTO orderDTO = orderService.findOne(ORDER_ID);
		OrderDTO result = orderService.finish(orderDTO);
		Assert.assertEquals(OrderStatusEnum.FINISHED.getCode(), result.getOrderStatus());
	}
	
	@Test
	public void paid() throws Exception {
		OrderDTO orderDTO = orderService.findOne(ORDER_ID);
		OrderDTO result = orderService.paid(orderDTO);
		Assert.assertEquals(PayStatusEnum.SUCCESS.getCode(), result.getPayStatus());
	}
	
	@Test
	public void list() {
		PageRequest request = new PageRequest(0, 2);
		Page <OrderDTO> orderDTOPage = orderService.findList(request);
		//	Assert.assertNotEquals(0, orderDTOPage.getTotalElements());
		Assert.assertTrue("查询所有订单列表", orderDTOPage.getTotalElements() > 0);
	}
}