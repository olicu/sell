package com.josehp.service.impl;

import com.josehp.dataobject.ProductInfo;
import com.josehp.enums.ProductStatusEnum;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceImplTest {
	@Autowired
	private ProductServiceImpl productService;
	
	@Test
	public void findOne() throws Exception {
		ProductInfo one = productService.findOne("123456");
		Assert.assertEquals("123456", one.getProductId());
	}
	
	@Test
	public void findUpAll() throws Exception {
		List <ProductInfo> upAll = productService.findUpAll();
		Assert.assertNotEquals(0, upAll.size());
	}
	
	@Test
	public void findAll() throws Exception {
		PageRequest request = new PageRequest(0, 2);
		Page <ProductInfo> productServiceAll = productService.findAll(request);
		System.out.println(productServiceAll.getTotalElements());
		
	}
	
	@Test
	public void save() throws Exception {
		ProductInfo productInfo = new ProductInfo();
		productInfo.setProductId("1234567");
		productInfo.setProductName("皮蛋粥2");
		productInfo.setProductPrice(new BigDecimal(3.3));
		productInfo.setProductStock(119);
		productInfo.setProductIcon("http://222.jpg");
		productInfo.setProductDescription("很好喝的粥1");
		productInfo.setProductStatus(ProductStatusEnum.DOWN.getCode());
		productInfo.setCategoryType(6);
		
		ProductInfo save = productService.save(productInfo);
		Assert.assertNotNull(save);
		
	}
	
}