package com.josehp.converter;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.josehp.dataobject.OrderDetail;
import com.josehp.dto.OrderDTO;
import com.josehp.enums.ResultEnum;
import com.josehp.exception.SellException;
import com.josehp.form.OrderForm;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Josehp
 * on 2018/2/20 0020  14:33.
 */
@Slf4j
public class OrderForm2OrderDTOConverter {
	
	public static OrderDTO convert(OrderForm orderForm) {
		Gson gson = new Gson();
		OrderDTO orderDTO = new OrderDTO();
		
		orderDTO.setBuyerName(orderForm.getName());
		orderDTO.setBuyerPhone(orderForm.getPhone());
		orderDTO.setBuyerAddress(orderForm.getAddress());
		orderDTO.setBuyerOpenid(orderForm.getOpenid());
		List <OrderDetail> orderDetailList = new ArrayList <>();
		try {
			orderDetailList = gson.fromJson(orderForm.getItems(),
					new TypeToken <List <OrderDetail>>() {
					}.getType());
		} catch (Exception e) {
			log.error("【对象转换】错误,json={}", orderForm.getItems());
			throw new SellException(ResultEnum.PARAM_ERROR);
		}
		
		orderDTO.setOrderDetailList(orderDetailList);
		return orderDTO;
	}
}
