package com.josehp.converter;

import com.josehp.dataobject.OrderMaster;
import com.josehp.dto.OrderDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: OrderMaster2OrderDTOConverter
 * @Package com.josehp.converter
 * @Description: 转换器
 * @Author Josehp
 * @CreateDate 2018/02/19 16:52
 * @Version V1.0
 */
public class OrderMaster2OrderDTOConverter {
	public static OrderDTO convert(OrderMaster orderMaster) {
		OrderDTO orderDTO = new OrderDTO();
		BeanUtils.copyProperties(orderMaster, orderDTO);
		return orderDTO;
	}
	
	public static List <OrderDTO> convert(List <OrderMaster> orderMasterList) {
		return orderMasterList.stream().map(e -> convert(e)).collect(Collectors.toList());
	}
}
