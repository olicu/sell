package com.josehp.exception;

import com.josehp.enums.ResultEnum;
import lombok.Getter;

/**
 * @ClassName: SellException
 * @Package com.josehp.exception
 * @Description:
 * @Author Josehp
 * @CreateDate 2018/02/19 13:42
 * @Version V1.0
 */
@Getter
public class SellException extends RuntimeException {
	
	private Integer code;
	
	public SellException(ResultEnum resultEnum) {
		super(resultEnum.getMessage());
		
		this.code = resultEnum.getCode();
	}
	
	public SellException(Integer code, String message) {
		super(message);
		this.code = code;
		
	}
}
