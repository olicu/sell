package com.josehp.dataobject;


import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * @ClassName: ProductCategory
 * @Package com.josehp.dataobject
 * @Description: 类目实体类
 * @Author Josehp
 * @CreateDate 2018/02/13 15:08
 * @Version V1.0
 */
@Entity
@DynamicUpdate
@Data
public class ProductCategory {
	
	/**
	 * 类目id.
	 */
	@Id
	@GeneratedValue
	private Integer categoryId;
	
	/**
	 * 类目名字.
	 */
	private String categoryName;
	
	/**
	 * 类目编号.
	 */
	private Integer categoryType;
	
	private Date createTime;
	private Date updateTime;
	
	public ProductCategory() {
	
	}
	
	public ProductCategory(String categoryName, Integer categoryType) {
		this.categoryName = categoryName;
		this.categoryType = categoryType;
	}
	
	
}
