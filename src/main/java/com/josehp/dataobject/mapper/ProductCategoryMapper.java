package com.josehp.dataobject.mapper;

import org.apache.ibatis.annotations.Insert;

import java.util.Map;

/**
 * Created by Josehp
 * on 2018/3/6 0006  20:45.
 */
public interface ProductCategoryMapper {
	@Insert("insert into product_category(category_name,category_type) values (#{category_name,jdbcType=VARCHAR},#{category_type,jdbcType=INTEGER})")
	int insertByMap(Map <String, Object> map);
	
}
