package com.josehp.dataobject;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @ClassName: SellerInfo
 * @Package com.josehp.dataobject
 * @Description: 买家信息
 * @Author Josehp
 * @CreateDate 2018/03/05 15:09
 * @Version V1.0
 */
@Data
@Entity
@DynamicUpdate
public class SellerInfo {
	@Id
	private String sellerId;
	
	private String username;
	
	private String password;
	
	private String openid;
	
	
}
