package com.josehp.dataobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.josehp.enums.ProductStatusEnum;
import com.josehp.utils.EnumUtil;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: ProductInfo
 * @Package com.josehp.dataobject
 * @Description: 商品DAO
 * @Author Josehp
 * @CreateDate 2018/02/13 15:34
 * @Version V1.0
 */
@Entity
@Data
@DynamicUpdate
public class ProductInfo {
	@Id
	private String productId;
	
	/**
	 * 名字
	 */
	private String productName;
	/**
	 * 单价
	 */
	private BigDecimal productPrice;
	
	/**
	 * 库存
	 */
	private Integer productStock;
	
	/**
	 * 描述
	 */
	private String productDescription;
	
	/**
	 * 小图
	 */
	private String productIcon;
	
	/**
	 * 商品状态,0正常1下架
	 */
	private Integer productStatus = ProductStatusEnum.DOWN.getCode();
	
	/**
	 * 类目编号
	 */
	private Integer categoryType;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改时间
	 */
	private Date updateTime;
	
	@JsonIgnore
	public ProductStatusEnum getProductStatusEnum() {
		return EnumUtil.getByCode(productStatus, ProductStatusEnum.class);
	}
	
}
