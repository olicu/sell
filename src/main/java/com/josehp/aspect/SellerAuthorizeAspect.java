package com.josehp.aspect;

import com.josehp.constant.CookieConstant;
import com.josehp.constant.RedisConstant;
import com.josehp.exception.SellerAuthorizeException;
import com.josehp.utils.CookieUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName: SellerAuthorizeAspect
 * @Package com.josehp.aspect
 * @Description: 卖家授权
 * @Author Josehp
 * @CreateDate 2018/03/06 13:44
 * @Version V1.0
 */
@Aspect
@Component
@Slf4j
public class SellerAuthorizeAspect {
	@Autowired
	private StringRedisTemplate redisTemplate;
	
	@Pointcut("execution(public * com.josehp.controller.Seller*.*(..))" +
			"&& !execution(public * com.josehp.controller.SellerUserController.*(..))")
	public void verify() {
	
	}
	
	;
	
	@Before("verify()")
	public void doVerify() {
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = attributes.getRequest();
		
		//查询cookie
		Cookie cookie = CookieUtil.get(request, CookieConstant.TOKEN);
		if ( cookie == null ) {
			log.warn("【登录校验】cookie中查不到token");
			throw new SellerAuthorizeException();
		}
		//redis查询
		String tokenValue = redisTemplate.opsForValue().get(String.format(RedisConstant.TOKEN_PREFIX, cookie.getValue()));
		if ( StringUtils.isEmpty(tokenValue) ) {
			log.warn("【登录校验】redis中查不到token");
			throw new SellerAuthorizeException();
			
		}
	}
	
}
