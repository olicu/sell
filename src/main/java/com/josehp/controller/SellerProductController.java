package com.josehp.controller;

import com.josehp.dataobject.ProductCategory;
import com.josehp.dataobject.ProductInfo;
import com.josehp.enums.ResultEnum;
import com.josehp.exception.SellException;
import com.josehp.form.ProductForm;
import com.josehp.repository.ProductCategoryRepository;
import com.josehp.service.ProductService;
import com.josehp.utils.KeyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: SellerProductController
 * @Package com.josehp.controller
 * @Description: 卖家端商品
 * @Author Josehp
 * @CreateDate 2018/03/03 20:37
 * @Version V1.0
 */
@Controller
@RequestMapping("/seller/product")
@Slf4j
public class SellerProductController {
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductCategoryRepository productCategoryRepository;
	
	@GetMapping("/list")
	public ModelAndView list(@RequestParam(value = "page", defaultValue = "1") Integer page,
	                         @RequestParam(value = "size", defaultValue = "10") Integer size,
	                         Map <String, Object> map) {
		PageRequest request = new PageRequest(page - 1, size);
		Page <ProductInfo> productInfoPage = productService.findAll(request);
		map.put("productInfoPage", productInfoPage);
		
		//当前页
		map.put("currentPage", page);
		
		map.put("size", size);
		
		return new ModelAndView("product/list", map);
		
	}
	
	
	/**
	 * 商品上架
	 *
	 * @param productId
	 * @param map
	 * @return
	 */
	@GetMapping("on_sale")
	public ModelAndView onSale(@RequestParam("productId") String productId,
	                           Map <String, Object> map) {
		try {
			productService.onSale(productId);
		} catch (SellException e) {
			map.put("msg", e.getMessage());
			map.put("url", "/sell/seller/product/list");
			return new ModelAndView("common/error", map);
			
		}
		map.put("msg", ResultEnum.PRODUCT_STATUS_UP_SUCCESS.getMessage());
		map.put("url", "/sell/seller/product/list");
		return new ModelAndView("common/success", map);
		
	}
	
	/**
	 * 商品下架
	 *
	 * @param productId
	 * @param map
	 * @return
	 */
	@RequestMapping("off_sale")
	public ModelAndView offSale(@RequestParam("productId") String productId,
	                            Map <String, Object> map) {
		try {
			productService.offSale(productId);
		} catch (SellException e) {
			map.put("msg", e.getMessage());
			map.put("url", "/sell/seller/product/list");
			return new ModelAndView("common/error", map);
			
		}
		map.put("msg", ResultEnum.PRODUCT_STATUS_DOWN_SUCCESS.getMessage());
		map.put("url", "/sell/seller/product/list");
		return new ModelAndView("common/success", map);
		
	}
	
	/**
	 * 商品修改
	 *
	 * @param productId
	 * @param map
	 * @return
	 */
	@GetMapping("/index")
	public ModelAndView index(@RequestParam(value = "productId", required = false) String productId,
	                          Map <String, Object> map) {
		if ( !StringUtils.isEmpty(productId) ) {
			ProductInfo productInfo = productService.findOne(productId);
			map.put("productInfo", productInfo);
			
		}
		
		//查询所有类目
		List <ProductCategory> categoryList = productCategoryRepository.findAll();
		map.put("categoryList", categoryList);
		
		return new ModelAndView("product/index", map);
	}
	
	/**
	 * 商品保存/更新
	 *
	 * @param form
	 * @param bindingResult
	 * @param map
	 * @return
	 */
	@PostMapping("/save")
	public ModelAndView save(@Valid ProductForm form,
	                         BindingResult bindingResult,
	                         Map <String, Object> map) {
		if ( bindingResult.hasErrors() ) {
			map.put("msg", bindingResult.getFieldError().getDefaultMessage());
			map.put("url", "/sell/seller/product/index");
			return new ModelAndView("common/error", map);
		}
		
		ProductInfo productInfo = new ProductInfo();
		try {
			//如果productId为空,说明时新增
			if ( !StringUtils.isEmpty(form.getProductId()) ) {
				productInfo = productService.findOne(form.getProductId());
			} else {
				form.setProductId(KeyUtil.genUniqueKey());
				
			}
			
			BeanUtils.copyProperties(form, productInfo);
			productService.save(productInfo);
		} catch (SellException e) {
			map.put("msg", e.getMessage());
			map.put("url", "/sell/seller/product/index");
			return new ModelAndView("common/error", map);
		}
		
		map.put("url", "/sell/seller/product/list");
		return new ModelAndView("common/success", map);
		
	}
}