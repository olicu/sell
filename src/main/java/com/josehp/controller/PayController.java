package com.josehp.controller;

import com.josehp.dto.OrderDTO;
import com.josehp.enums.ResultEnum;
import com.josehp.exception.SellException;
import com.josehp.service.OrderService;
import com.josehp.service.PayService;
import com.lly835.bestpay.model.PayResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * @ClassName: PayController
 * @Package com.josehp.controller
 * @Description: 微信支付
 * @Author Josehp
 * @CreateDate 2018/03/02 17:10
 * @Version V1.0
 */
@Controller
@RequestMapping("/pay")
@Slf4j
public class PayController {
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private PayService payService;
	
	@GetMapping("/create")
	public ModelAndView create(@RequestParam("orderId") String orderId,
	                           @RequestParam("returnUrl") String returnUrl,
	                           Map <String, Object> map) {
		
		//1.查询订单
		OrderDTO orderDTO = orderService.findOne(orderId);
		if ( orderDTO == null ) {
			throw new SellException(ResultEnum.ORDER_NOT_EXIT);
			
		}
		//2.发起支付
		PayResponse payResponse = payService.create(orderDTO);
		map.put("payResponse", payResponse);
		map.put("returnUrl", returnUrl);
		return new ModelAndView("pay/create", map);
	}
	
	@PostMapping("/notify")
	public ModelAndView notify(@RequestBody String notifyData) {
		
		payService.notify(notifyData);
		
		//返回给微信处理结果
		return new ModelAndView("pay/success");
	}
	
	
}
