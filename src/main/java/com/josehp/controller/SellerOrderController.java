package com.josehp.controller;

import com.josehp.dto.OrderDTO;
import com.josehp.enums.ResultEnum;
import com.josehp.exception.SellException;
import com.josehp.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * @ClassName: SellerOrderController
 * @Package com.josehp.controller
 * @Description: 卖家端订单
 * @Author Josehp
 * @CreateDate 2018/03/03 15:05
 * @Version V1.0
 */
@Controller
@Slf4j
@RequestMapping("/seller/order")
public class SellerOrderController {
	@Autowired
	private OrderService orderService;
	
	/**
	 * 订单列表
	 *
	 * @param page 第几页,从1页开始
	 * @param size 一页有多少条数据
	 * @return
	 */
	@GetMapping("/list")
	public ModelAndView list(@RequestParam(value = "page", defaultValue = "1") Integer page,
	                         @RequestParam(value = "size", defaultValue = "10") Integer size,
	                         Map <String, Object> map) {
		PageRequest request = new PageRequest(page - 1, size);
		Page <OrderDTO> orderDTOPage = orderService.findList(request);
		map.put("orderDTOPage", orderDTOPage);
		map.put("currentPage", page);
		map.put("size", size);
		return new ModelAndView("order/list", map);
	}
	
	/**
	 * 取消订单
	 *
	 * @param orderId
	 * @return map
	 */
	@GetMapping("/cancel")
	public ModelAndView cancel(@RequestParam("orderId") String orderId,
	                           Map <String, Object> map) {
		try {
			OrderDTO orderDTO = orderService.findOne(orderId);
			orderService.cancel(orderDTO);
		} catch (SellException e) {
			log.error("【卖家端取消订单】发生异常{}", e);
			map.put("msg", e.getMessage());
			map.put("url", "/sell/seller/order/list");
			return new ModelAndView("common/error", map);
		}
		map.put("msg", ResultEnum.ORDER_CANCEL_SUCCESS.getMessage());
		map.put("url", "/sell/seller/order/list");
		return new ModelAndView("common/success");
	}
	
	/**
	 * 订单详情
	 *
	 * @param orderId
	 * @return map
	 */
	@GetMapping("/detail")
	public ModelAndView detail(@RequestParam("orderId") String orderId,
	                           Map <String, Object> map) {
		OrderDTO orderDTO = new OrderDTO();
		try {
			orderDTO = orderService.findOne(orderId);
		} catch (SellException e) {
			log.error("【卖家端查询订单详情】发生异常{}", e);
			map.put("msg", e.getMessage());
			map.put("url", "/sell/seller/order/list");
			return new ModelAndView("common/error", map);
		}
		map.put("orderDTO", orderDTO);
		return new ModelAndView("order/detail", map);
		
	}
	
	@GetMapping("/finish")
	public ModelAndView finish(@RequestParam("orderId") String orderId,
	                           Map <String, Object> map) {
		try {
			OrderDTO orderDTO = orderService.findOne(orderId);
			orderService.finish(orderDTO);
		} catch (SellException e) {
			log.error("【卖家端完结订单】 发生异常{}", e);
			
			map.put("msg", e.getMessage());
			map.put("url", "/sell/seller/order/list");
			return new ModelAndView("common/error", map);
		}
		map.put("msg", ResultEnum.ORDER_FINISH_SUCCESS.getMessage());
		map.put("url", "/sell/seller/order/list");
		return new ModelAndView("common/success");
		
	}
}
