package com.josehp.controller;

import com.josehp.VO.ProductInfoVO;
import com.josehp.VO.ProductVO;
import com.josehp.VO.ResultVO;
import com.josehp.dataobject.ProductCategory;
import com.josehp.dataobject.ProductInfo;
import com.josehp.service.CategoryService;
import com.josehp.service.ProductService;
import com.josehp.utils.ResultVOUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: BuyerProductController
 * @Package com.josehp.controller
 * @Description: 买家商品
 * @Author Josehp
 * @CreateDate 2018/02/13 16:57
 * @Version V1.0
 */
@RestController
@RequestMapping("/buyer/product")
public class BuyerProductController {
	
	@Autowired
	private ProductService productService;
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/list")
	public ResultVO list() {
		//查询所有上架的商品
		List <ProductInfo> productInfos = productService.findUpAll();
		//List<Integer> categoryTypeList = new ArrayList <>();
		/*for ( ProductInfo productInfo : productInfos ) {
			categoryTypeList.add(productInfo.getCategoryType());
		}*/
		//经典做法 java8  lambda
		//查询类目 一次性查询
		List <Integer> categoryTypeList = productInfos.stream()
				.map(e -> e.getCategoryType())
				.collect(Collectors.toList());
		List <ProductCategory> productCategories = categoryService.findByCategoryTypeIn(categoryTypeList);
		//封装数据
		List <ProductVO> productVOList = new ArrayList <>();
		for ( ProductCategory productCategory : productCategories ) {
			ProductVO productVO = new ProductVO();
			productVO.setCategoryType(productCategory.getCategoryType());
			productVO.setCategoryName(productCategory.getCategoryName());
			
			List <ProductInfoVO> productInfoVOList = new ArrayList <>();
			
			for ( ProductInfo productInfo : productInfos ) {
				if ( productInfo.getCategoryType().equals(productCategory.getCategoryType()) ) {
					ProductInfoVO productInfoVO = new ProductInfoVO();
					BeanUtils.copyProperties(productInfo, productInfoVO);//数据拷贝
					productInfoVOList.add(productInfoVO);
				}
			}
			productVO.setProductInfoVOList(productInfoVOList);
			productVOList.add(productVO);
		}
		/*ResultVO resultVO = new ResultVO();*/
	/*	ProductVO productVO = new ProductVO();
		ProductInfoVO productInfoVO = new ProductInfoVO();
		productVO.setProductInfoVOList(Arrays.asList(productInfoVO));
		resultVO.setData(Arrays.asList(productVO));*/
	/*	resultVO.setData(productVOList);
		resultVO.setCode(0);
		resultVO.setMsg("成功");*/
		
		return ResultVOUtil.success(productVOList);
	}
}
