package com.josehp.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Josehp
 * on 2018/2/26 0026  15:39.
 */
@RestController
@RequestMapping("/weixin")
@Slf4j
public class WeixinController {
	@GetMapping("/auth")
	public void auth(@RequestParam("code") String code) {
		log.info("进入auth方法....");
		log.info("code={}", code);
		String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=wxcda15f8f6fc46cb7&secret=195e0d959724c1a5b0af7204dea66843&code=" + code + "&grant_type=authorization_code";
		RestTemplate restTemplate = new RestTemplate();
		String response = restTemplate.getForObject(url, String.class);
		log.info("response={}", response);
		
	}
	
}
