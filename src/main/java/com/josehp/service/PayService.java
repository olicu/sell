package com.josehp.service;

import com.josehp.dto.OrderDTO;
import com.lly835.bestpay.model.PayResponse;
import com.lly835.bestpay.model.RefundResponse;

/**
 * @ClassName: PayService
 * @Package com.josehp.service
 * @Description: 微信支付service
 * @Author Josehp
 * @CreateDate 2018/03/02 17:13
 * @Version V1.0
 */
public interface PayService {
	
	PayResponse create(OrderDTO orderDTO);
	
	PayResponse notify(String notifyData);
	
	RefundResponse refund(OrderDTO orderDTO);
}
