package com.josehp.service;

import com.josehp.dataobject.ProductInfo;
import com.josehp.dto.CartDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import java.util.List;

/**
 * @ClassName: ProductService
 * @Package com.josehp.service
 * @Description: 商品service
 * @Author Josehp
 * @CreateDate 2018/02/13 16:27
 * @Version V1.0
 */
public interface ProductService {
	
	ProductInfo findOne(String productId);
	
	/**
	 * 查询所有在架商品类表
	 *
	 * @return
	 */
	List <ProductInfo> findUpAll();
	
	Page <ProductInfo> findAll(Pageable pageable);
	
	ProductInfo save(ProductInfo productInfo);
	
	//加库存
	void increaseStock(List <CartDTO> cartDTOList);
	
	//减库存
	void decreaseStock(List <CartDTO> cartDTOList);
	
	//上架
	ProductInfo onSale(String productId);
	
	//下架
	ProductInfo offSale(String productId);
}
