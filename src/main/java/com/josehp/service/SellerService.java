package com.josehp.service;

import com.josehp.dataobject.SellerInfo;

/**
 * @ClassName: SellerService
 * @Package com.josehp.service
 * @Description: 卖家端
 * @Author Josehp
 * @CreateDate 2018/03/05 15:28
 * @Version V1.0
 */

public interface SellerService {
	/**
	 * 通过openid查询卖家端信息
	 *
	 * @param openid
	 * @return
	 */
	SellerInfo findSellerInfoByOpenid(String openid);
}
