package com.josehp.service;

import com.josehp.dto.OrderDTO;

/**
 * @ClassName: BuyerService
 * @Package com.josehp.service
 * @Description:
 * @Author Josehp
 * @CreateDate 2018/02/20 15:51
 * @Version V1.0
 */
public interface BuyerService {
	//查询一个订单
	OrderDTO findOrderOne(String openid, String orderid);
	
	//取消订单
	OrderDTO cancelOrder(String openid, String orderid);
	
}
