package com.josehp.service.impl;

import com.josehp.dataobject.SellerInfo;
import com.josehp.repository.SellerInfoRepository;
import com.josehp.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName: SellerServiceImpl
 * @Package com.josehp.service.impl
 * @Description:
 * @Author Josehp
 * @CreateDate 2018/03/05 15:29
 * @Version V1.0
 */
@Service
public class SellerServiceImpl implements SellerService {
	@Autowired
	private SellerInfoRepository repository;
	
	@Override
	public SellerInfo findSellerInfoByOpenid(String openid) {
		return repository.findByOpenid(openid);
	}
}
