package com.josehp.service.impl;

import com.josehp.dto.OrderDTO;
import com.josehp.enums.ResultEnum;
import com.josehp.exception.SellException;
import com.josehp.service.BuyerService;
import com.josehp.service.OrderService;
import com.sun.org.apache.regexp.internal.RE;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Josehp
 * on 2018/2/20 0020  15:53.
 */
@Service
@Slf4j
public class BuyerServiceImpl implements BuyerService {
	@Autowired
	private OrderService orderService;
	
	@Override
	public OrderDTO findOrderOne(String openid, String orderid) {
		return checkOrderOwner(openid, orderid);
	}
	
	@Override
	public OrderDTO cancelOrder(String openid, String orderid) {
		OrderDTO orderDTO = checkOrderOwner(openid, orderid);
		if ( orderDTO == null ) {
			log.error("【取消订单】查不到该订单,orderId={}", openid);
			throw new SellException(ResultEnum.ORDER_NOT_EXIT);
		}
		
		return orderService.cancel(orderDTO);
	}
	
	private OrderDTO checkOrderOwner(String openid, String orderid) {
		OrderDTO orderDTO = orderService.findOne(orderid);
		if ( orderDTO == null ) {
			return null;
		}
		//判断是否是自己的订单
		if ( !orderDTO.getBuyerOpenid().equalsIgnoreCase(openid) ) {
			log.error("【查询订单】订单的openid不一致,openid={},orderDTO={}", openid, orderDTO);
			throw new SellException(ResultEnum.ORDER_OWNER_ERROR);
		}
		return orderDTO;
	}
}
