package com.josehp.service.impl;

import com.josehp.converter.OrderMaster2OrderDTOConverter;
import com.josehp.dataobject.OrderDetail;
import com.josehp.dataobject.OrderMaster;
import com.josehp.dataobject.ProductInfo;
import com.josehp.dto.CartDTO;
import com.josehp.dto.OrderDTO;

import com.josehp.enums.OrderStatusEnum;
import com.josehp.enums.PayStatusEnum;
import com.josehp.enums.ResultEnum;
import com.josehp.exception.SellException;
import com.josehp.repository.OrderDetailRepository;
import com.josehp.repository.OrderMasterRepository;
import com.josehp.service.*;
import com.josehp.utils.KeyUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;


import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName: OrderServiceImpl
 * @Package com.josehp.service.impl
 * @Description:
 * @Author Josehp
 * @CreateDate 2018/02/15 16:53
 * @Version V1.0
 */
@Service
@Slf4j
public class OrderServiceImpl implements OrderService {
	@Autowired
	private ProductService productService;
	
	@Autowired
	private OrderDetailRepository orderDetailRepository;
	
	@Autowired
	private OrderMasterRepository orderMasterRepository;
	
	@Autowired
	private PayService payService;
	
	@Autowired
	private PushMessageService pushMessageService;
	
	@Autowired
	private WebSocket webSocket;
	
	@Override
	@Transactional
	public OrderDTO create(OrderDTO orderDTO) {
		BigDecimal orderAmount = new BigDecimal(0);
		String orderId = KeyUtil.genUniqueKey();
		
		//写入顺序问题
		//3.写入订单(orderMaster和orderDetail)
		OrderMaster orderMaster = new OrderMaster();
		orderDTO.setOrderId(orderId);
		BeanUtils.copyProperties(orderDTO, orderMaster);
		orderMaster.setOrderAmount(orderAmount);
		orderMaster.setOrderStatus(OrderStatusEnum.NEW.getCode());
		orderMaster.setPayStatus(PayStatusEnum.WAIT.getCode());
		orderMasterRepository.save(orderMaster);
		
		//1.查找商品列表
		for ( OrderDetail orderDetail : orderDTO.getOrderDetailList() ) {
			ProductInfo productInfo = productService.findOne(orderDetail.getProductId());
			if ( null == productInfo ) {
				//没有该商品，抛出异常
				throw new SellException(ResultEnum.PRODUCT_NOT_EXIT);
			}
			
			//2.计算订单总额
			orderAmount = productInfo.getProductPrice()
					.multiply(new BigDecimal(orderDetail.getProductQuantity()))
					.add(orderAmount);
			//3.订单详情入库
			BeanUtils.copyProperties(productInfo, orderDetail);
			orderDetail.setDetailId(KeyUtil.genUniqueKey());
			orderDetail.setOrderId(orderId);
			orderDetailRepository.save(orderDetail);
		}
		orderMaster.setOrderAmount(orderAmount);
		orderMasterRepository.save(orderMaster);
		
		//4.出库
		List <CartDTO> cartDTOList = orderDTO.getOrderDetailList().stream()
				.map(e -> new CartDTO(e.getProductId(), e.getProductQuantity()))
				.collect(Collectors.toList());
		productService.decreaseStock(cartDTOList);
		
		//发送websocket消息
		
		webSocket.sendMessage(orderDTO.getOrderId());
		return orderDTO;
		
	}
	
	@Override
	public OrderDTO findOne(String orderId) {
		
		OrderMaster orderMaster = orderMasterRepository.findOne(orderId);
		if ( orderMaster == null ) {
			throw new SellException(ResultEnum.ORDER_NOT_EXIT);
		}
		List <OrderDetail> orderDetailList = orderDetailRepository.findByOrderId(orderId);
		if ( CollectionUtils.isEmpty(orderDetailList) ) {
			throw new SellException(ResultEnum.ORDERDETIAL_NOT_EXIT);
		}
		OrderDTO orderDTO = new OrderDTO();
		BeanUtils.copyProperties(orderMaster, orderDTO);
		orderDTO.setOrderDetailList(orderDetailList);
		
		return orderDTO;
	}
	
	@Override
	public Page <OrderDTO> findList(String buyerOpenid, Pageable pageable) {
		Page <OrderMaster> orderMasterPage = orderMasterRepository.findByBuyerOpenid(buyerOpenid, pageable);
		
		List <OrderDTO> orderDTOList = OrderMaster2OrderDTOConverter.convert(orderMasterPage.getContent());
		
		Page <OrderDTO> orderDTOPage = new PageImpl <OrderDTO>(orderDTOList, pageable, orderMasterPage.getTotalElements());
		
		return orderDTOPage;
	}
	
	@Override
	@Transactional
	public OrderDTO cancel(OrderDTO orderDTO) {
		OrderMaster orderMaster = new OrderMaster();
		
		//1.判断订单的状态
		if ( !orderDTO.getOrderStatus().equals(OrderStatusEnum.NEW.getCode()) ) {
			log.error("【取消订单】 订单状态不正确,orderId={},orderStatus={}", orderDTO.getOrderId(), orderDTO.getOrderStatus());
			throw new SellException(ResultEnum.ORDER_STATUS_ERROR);
		}
		//2.修改订单状态
		orderDTO.setOrderStatus(OrderStatusEnum.CANCEL.getCode());
		BeanUtils.copyProperties(orderDTO, orderMaster);
		OrderMaster updateResult = orderMasterRepository.save(orderMaster);
		
		if ( updateResult == null ) {
			log.error("【取消订单】更新失败,orderMaster={}", orderMaster);
			throw new SellException(ResultEnum.ORDER_UPDATE_FAIL);
		}
		//3.返回库存
		if ( CollectionUtils.isEmpty(orderDTO.getOrderDetailList()) ) {
			log.error("【取消订单】订单中无商品详情,orderDTO={}", orderDTO);
		}
		
		List <CartDTO> cartDTOList = orderDTO.getOrderDetailList().stream().map(e ->
				new CartDTO(e.getProductId(), e.getProductQuantity())
		).collect(Collectors.toList());
		
		productService.increaseStock(cartDTOList);
		//4.退款 如果已支付
		if ( orderDTO.getPayStatus().equals(PayStatusEnum.SUCCESS.getCode()) ) {
			payService.refund(orderDTO);
		}
		return orderDTO;
	}
	
	@Override
	@Transactional
	public OrderDTO finish(OrderDTO orderDTO) {
		
		//判断订单状态
		if ( orderDTO.getOrderStatus().equals(OrderStatusEnum.FINISHED.getCode()) ) {
			log.error("【完结订单】订单状态不正确,orderId={},orderStatus={}", orderDTO.getOrderId(), orderDTO.getOrderStatus());
			throw new SellException(ResultEnum.ORDER_STATUS_ERROR);
		}
		
		//修改订单状态
		orderDTO.setOrderStatus(OrderStatusEnum.FINISHED.getCode());
		OrderMaster orderMaster = new OrderMaster();
		BeanUtils.copyProperties(orderDTO, orderMaster);
		
		OrderMaster result = orderMasterRepository.save(orderMaster);
		if ( result == null ) {
			log.error("【完结订单】更新失败,orderMaster={}", orderMaster);
			throw new SellException(ResultEnum.ORDER_UPDATE_FAIL);
		}
		//推送微信模板消息
		pushMessageService.orderStatus(orderDTO);
		return orderDTO;
	}
	
	@Override
	@Transactional
	public OrderDTO paid(OrderDTO orderDTO) {
		//判断订单状态
		//1.判断订单的状态
		if ( !orderDTO.getOrderStatus().equals(OrderStatusEnum.NEW.getCode()) ) {
			log.error("【订单支付完成】 订单状态不正确,orderId={},orderStatus={}", orderDTO.getOrderId(), orderDTO.getOrderStatus());
			throw new SellException(ResultEnum.ORDER_STATUS_ERROR);
		}
		//判断支付状态
		if ( !orderDTO.getPayStatus().equals(PayStatusEnum.WAIT.getCode()) ) {
			log.error("【订单支付完成】 订单支付状态不正确,order={}", orderDTO);
			throw new SellException(ResultEnum.ORDER_PAY_STATUS_ERROR);
		}
		//修改支付状态
		orderDTO.setPayStatus(PayStatusEnum.SUCCESS.getCode());
		OrderMaster orderMaster = new OrderMaster();
		BeanUtils.copyProperties(orderDTO, orderMaster);
		
		OrderMaster result = orderMasterRepository.save(orderMaster);
		if ( result == null ) {
			log.error("【订单支付完成】更新失败,orderMaster={}", orderMaster);
			throw new SellException(ResultEnum.ORDER_UPDATE_FAIL);
		}
		
		return orderDTO;
	}
	
	@Override
	public Page <OrderDTO> findList(Pageable pageable) {
		
		Page <OrderMaster> orderMasterPage = orderMasterRepository.findAll(pageable);
		
		
		List <OrderDTO> orderDTOList = OrderMaster2OrderDTOConverter.convert(orderMasterPage.getContent());
		
		Page <OrderDTO> orderDTOPage = new PageImpl <OrderDTO>(orderDTOList, pageable, orderMasterPage.getTotalElements());
		
		return orderDTOPage;
	}
}
