package com.josehp.service.impl;

import com.josehp.dataobject.ProductCategory;
import com.josehp.repository.ProductCategoryRepository;
import com.josehp.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: CategoryServiceImpl
 * @Package com.josehp.service.impl
 * @Description:
 * @Author Josehp
 * @CreateDate 2018/02/13 15:17
 * @Version V1.0
 */
@Service
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	private ProductCategoryRepository repository;
	
	@Override
	public ProductCategory findOne(Integer categoryId) {
		return repository.findOne(categoryId);
	}
	
	@Override
	public List <ProductCategory> findAll() {
		return repository.findAll();
	}
	
	@Override
	public List <ProductCategory> findByCategoryTypeIn(List <Integer> categoryTypeList) {
		return repository.findByCategoryTypeIn(categoryTypeList);
	}
	
	@Override
	public ProductCategory save(ProductCategory productCategory) {
		return repository.save(productCategory);
	}
}
