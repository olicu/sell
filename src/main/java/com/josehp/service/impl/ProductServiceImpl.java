package com.josehp.service.impl;

import com.josehp.dataobject.ProductInfo;
import com.josehp.dto.CartDTO;
import com.josehp.enums.ProductStatusEnum;
import com.josehp.enums.ResultEnum;
import com.josehp.exception.SellException;
import com.josehp.repository.ProductInfoRepository;
import com.josehp.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.util.List;

/**
 * @ClassName: ProductServiceImpl
 * @Package com.josehp.service.impl
 * @Description: 商品service实现类
 * @Author Josehp
 * @CreateDate 2018/02/13 16:30
 * @Version V1.0
 */
@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductInfoRepository repository;
	
	@Override
	public ProductInfo findOne(String productId) {
		return repository.findOne(productId);
	}
	
	@Override
	public List <ProductInfo> findUpAll() {
		return repository.findByProductStatus(ProductStatusEnum.UP.getCode());
	}
	
	@Override
	public Page <ProductInfo> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}
	
	@Override
	public ProductInfo save(ProductInfo productInfo) {
		return repository.save(productInfo);
	}
	
	@Override
	@Transactional
	public void increaseStock(List <CartDTO> cartDTOList) {
		
		for ( CartDTO cartDTO : cartDTOList ) {
			ProductInfo productInfo = repository.findOne(cartDTO.getProductId());
			if ( productInfo == null ) {
				throw new SellException(ResultEnum.PRODUCT_NOT_EXIT);
			}
			Integer result = productInfo.getProductStock() + cartDTO.getProductQuantity();
			
			productInfo.setProductStock(result);
			repository.save(productInfo);
		}
	}
	
	@Override
	@Transactional
	public void decreaseStock(List <CartDTO> cartDTOList) {
		for ( CartDTO cartDTO : cartDTOList ) {
			ProductInfo productInfo = repository.findOne(cartDTO.getProductId());
			if ( productInfo == null ) {
				throw new SellException(ResultEnum.PRODUCT_NOT_EXIT);
			}
			Integer result = productInfo.getProductStock() - cartDTO.getProductQuantity();
			if ( result < 0 ) {
				throw new SellException(ResultEnum.PRODUCT_STOCK_ERROR);
			}
			productInfo.setProductStock(result);
			repository.save(productInfo);
		}
	}
	
	@Override
	public ProductInfo onSale(String productId) {
		ProductInfo productInfo = repository.findOne(productId);
		if ( productInfo == null ) {
			throw new SellException(ResultEnum.PRODUCT_NOT_EXIT);
		}
		if ( productInfo.getProductStatusEnum() == ProductStatusEnum.UP ) {
			throw new SellException(ResultEnum.PRODUCT_STATUS_ERROR);
		}
		//更新
		productInfo.setProductStatus(ProductStatusEnum.UP.getCode());
		return repository.save(productInfo);
		
	}
	
	@Override
	public ProductInfo offSale(String productId) {
		ProductInfo productInfo = repository.findOne(productId);
		if ( productInfo == null ) {
			throw new SellException(ResultEnum.PRODUCT_NOT_EXIT);
		}
		if ( productInfo.getProductStatusEnum() == ProductStatusEnum.DOWN ) {
			throw new SellException(ResultEnum.PRODUCT_STATUS_ERROR);
		}
		//更新
		productInfo.setProductStatus(ProductStatusEnum.DOWN.getCode());
		return repository.save(productInfo);
	}
}
