package com.josehp.service;

import com.josehp.dto.OrderDTO;

/**
 * @ClassName: PushMessage
 * @Package com.josehp.service
 * @Description: 消息推送
 * @Author Josehp
 * @CreateDate 2018/03/06 14:19
 * @Version V1.0
 */
public interface PushMessageService {
	/**
	 * 订单状态变更消息
	 *
	 * @param orderDTO
	 */
	void orderStatus(OrderDTO orderDTO);
}
