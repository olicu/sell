package com.josehp.service;

import com.josehp.dataobject.ProductCategory;

import java.util.List;

/**
 * @ClassName: CategoryService
 * @Package com.josehp.service
 * @Description: 类目service
 * @Author Josehp
 * @CreateDate 2018/02/13 15:13
 * @Version V1.0
 */
public interface CategoryService {
	ProductCategory findOne(Integer categoryId);
	
	List <ProductCategory> findAll();
	
	List <ProductCategory> findByCategoryTypeIn(List <Integer> categoryTypeList);
	
	ProductCategory save(ProductCategory productCategory);
	
	
}
