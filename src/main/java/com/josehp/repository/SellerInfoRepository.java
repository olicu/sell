package com.josehp.repository;

import com.josehp.dataobject.SellerInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @ClassName: SellerInfoRepository
 * @Package com.josehp.repository
 * @Description: 买家dao
 * @Author Josehp
 * @CreateDate 2018/03/05 15:12
 * @Version V1.0
 */
public interface SellerInfoRepository extends JpaRepository <SellerInfo, String> {
	SellerInfo findByOpenid(String openid);
}
