package com.josehp.repository;

import com.josehp.dataobject.ProductInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @ClassName: ProductInfoRepository
 * @Package com.josehp.repository
 * @Description: 商品Dao接口
 * @Author Josehp
 * @CreateDate 2018/02/13 16:07
 * @Version V1.0
 */
public interface ProductInfoRepository extends JpaRepository <ProductInfo, String> {
	
	List <ProductInfo> findByProductStatus(Integer productStatus);
	
	
}
