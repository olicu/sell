package com.josehp.repository;

import com.josehp.dataobject.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @ClassName: ProductCategoryRepository
 * @Package com.josehp.repository
 * @Description: 类目接口
 * @Author Josehp
 * @CreateDate 2018/02/13 15:08
 * @Version V1.0
 */
public interface ProductCategoryRepository extends JpaRepository <ProductCategory, Integer> {
	List <ProductCategory> findByCategoryTypeIn(List <Integer> categoryList);
}
