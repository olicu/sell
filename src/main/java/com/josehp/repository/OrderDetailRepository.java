package com.josehp.repository;

import com.josehp.dataobject.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @ClassName: OrderDetailRepository
 * @Package com.josehp.repository
 * @Description: 订单详情dao
 * @Author Josehp
 * @CreateDate 2018/02/15 0:28
 * @Version V1.0
 */
public interface OrderDetailRepository extends JpaRepository <OrderDetail, String> {
	List <OrderDetail> findByOrderId(String orderId);
}
