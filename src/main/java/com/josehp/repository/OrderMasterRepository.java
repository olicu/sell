package com.josehp.repository;

import com.josehp.dataobject.OrderMaster;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @ClassName: OrderMasterRepository
 * @Package com.josehp.repository
 * @Description: 订单dao
 * @Author Josehp
 * @CreateDate 2018/02/15 0:24
 * @Version V1.0
 */
public interface OrderMasterRepository extends JpaRepository <OrderMaster, String> {
	Page <OrderMaster> findByBuyerOpenid(String buyerOpenid, Pageable pageable);
	
}
