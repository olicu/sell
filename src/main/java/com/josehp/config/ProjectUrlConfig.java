package com.josehp.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by Josehp
 * on 2018/3/5 0005  15:57.
 */
@Data
@ConfigurationProperties(prefix = "projectUrl")
@Component
public class ProjectUrlConfig {
	/**
	 * 微信公众平台授权url
	 */
	public String wechatMpAuthorize;
	/**
	 * 微信开发平台授权url
	 */
	private String wechatOpenAuthorize;
	/**
	 * 点餐系统
	 */
	public String sell;
}
