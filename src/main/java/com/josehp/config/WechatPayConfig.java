package com.josehp.config;

import com.lly835.bestpay.config.WxPayH5Config;
import com.lly835.bestpay.service.impl.BestPayServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @ClassName: WechatPayConfig
 * @Package com.josehp.config
 * @Description: 微信支付配置
 * @Author Josehp
 * @CreateDate 2018/03/02 17:20
 * @Version V1.0
 */
@Component
public class WechatPayConfig {
	
	@Autowired
	private WechatAccountConfig accountConfig;
	
	@Bean
	public BestPayServiceImpl bestPayService() {
		BestPayServiceImpl bestPayService = new BestPayServiceImpl();
		bestPayService.setWxPayH5Config(wxPayH5Config());
		return bestPayService;
	}
	
	@Bean
	public WxPayH5Config wxPayH5Config() {
		WxPayH5Config wxPayH5Config = new WxPayH5Config();
		wxPayH5Config.setAppId(accountConfig.getMpAppId());
		wxPayH5Config.setMchId(accountConfig.getMchId());
		wxPayH5Config.setAppSecret(accountConfig.getMpAppSecret());
		wxPayH5Config.setKeyPath(accountConfig.getKeyPath());
		wxPayH5Config.setMchKey(accountConfig.getMchKey());
		wxPayH5Config.setNotifyUrl(accountConfig.getNotifyUrl());
		return wxPayH5Config;
	}
}
