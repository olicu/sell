package com.josehp.handler;

import com.josehp.VO.ResultVO;
import com.josehp.config.ProjectUrlConfig;
import com.josehp.exception.SellException;
import com.josehp.exception.SellerAuthorizeException;
import com.josehp.utils.ResultVOUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Josehp
 * on 2018/3/6 0006  13:56.
 */
@ControllerAdvice
public class SellExceptionHandler {
	@Autowired
	private ProjectUrlConfig projectUrlConfig;
	
	//拦截登录异常
	@ExceptionHandler(value = SellerAuthorizeException.class)
	public ModelAndView handlerAuthorizeException() {
		return new ModelAndView("redirect:"
				.concat(projectUrlConfig.getWechatOpenAuthorize())//授权地址
				.concat("/sell/wechat/qrAuthorize")
				.concat("?returnUrl=")
				.concat(projectUrlConfig.getSell())
				.concat("/sell/seller/login"));
		
	}
	
	@ExceptionHandler(value = SellException.class)
	@ResponseBody
	public ResultVO handlerSellerException(SellException e) {
		return ResultVOUtil.error(e.getCode(), e.getMessage());
	}
}
