package com.josehp.form;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName: ProductForm
 * @Package com.josehp.form
 * @Description: 商品验证
 * @Author Josehp
 * @CreateDate 2018/03/03 21:46
 * @Version V1.0
 */
@Data
public class ProductForm {
	
	/**
	 * 商品主键id
	 */
	private String productId;
	
	/**
	 * 商品名称
	 */
	private String productName;
	
	/**
	 * 单价
	 */
	private BigDecimal productPrice;
	
	/**
	 * 库存
	 */
	private Integer productStock;
	
	/**
	 * 描述
	 */
	private String productDescription;
	
	/**
	 * 商品小图
	 */
	private String productIcon;
	
	/**
	 * 类目编号
	 */
	private Integer categoryType;
}
