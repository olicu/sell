package com.josehp.form;

import lombok.Data;

/**
 * @ClassName: CategoryForm
 * @Package com.josehp.form
 * @Description: 类目表单
 * @Author Josehp
 * @CreateDate 2018/03/03 22:23
 * @Version V1.0
 */
@Data
public class CategoryForm {
	private Integer categoryId;
	
	/**
	 * 类目名字。
	 */
	private String categoryName;
	
	/**
	 * 类目编号
	 */
	private Integer categoryType;
}
