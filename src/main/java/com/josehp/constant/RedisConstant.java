package com.josehp.constant;

/**
 * @ClassName: RedisConstant
 * @Package com.josehp.constant
 * @Description: redis常量
 * @Author Josehp
 * @CreateDate 2018/03/06 0:04
 * @Version V1.0
 */
public interface RedisConstant {
	String TOKEN_PREFIX = "token_%s";
	Integer EXPIRE = 7200;//2小时
}
