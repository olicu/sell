package com.josehp.constant;

/**
 * @ClassName: CookieConstant
 * @Package com.josehp.constant
 * @Description: cookie常量
 * @Author Josehp
 * @CreateDate 2018/03/06 0:22
 * @Version V1.0
 */
public interface CookieConstant {
	String TOKEN = "token";
	Integer EXPIRE = 7200;
}
