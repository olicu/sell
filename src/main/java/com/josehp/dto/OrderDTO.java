package com.josehp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.josehp.dataobject.OrderDetail;
import com.josehp.enums.OrderStatusEnum;
import com.josehp.enums.PayStatusEnum;
import com.josehp.utils.EnumUtil;
import com.josehp.utils.serializer.Date2LongSerializer;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Josehp
 * on 2018/2/15 0015  16:46.
 */
@Data
//@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)//字段为空不传
@JsonInclude(JsonInclude.Include.NON_NULL) //单独  全局的在配置文件里设置
public class OrderDTO {
	/**
	 * 订单id
	 */
	private String orderId;
	
	/**
	 * 买家名字
	 */
	private String buyerName;
	
	/**
	 * 买家手机号
	 */
	private String buyerPhone;
	
	/**
	 * 买家地址
	 */
	private String buyerAddress;
	
	/**
	 * 买家微信Openid
	 */
	private String buyerOpenid;
	
	/**
	 * 订单总金额
	 */
	private BigDecimal orderAmount;
	
	/**
	 * 订单状态，默认为0新下单
	 */
	private Integer orderStatus;
	
	/**
	 * 支付状态，默认为0未支付
	 */
	private Integer payStatus;
	/**
	 * 创建时间
	 */
	@JsonSerialize(using = Date2LongSerializer.class)//对时间单独处理 /1000
	private Date createTime;
	/**
	 * 更新时间
	 */
	@JsonSerialize(using = Date2LongSerializer.class)
	private Date updateTime;
	
	private List <OrderDetail> orderDetailList;
	
	@JsonIgnore//忽略
	public OrderStatusEnum getOrderStatusEnum() {
		return EnumUtil.getByCode(orderStatus, OrderStatusEnum.class);
	}
	
	@JsonIgnore
	public PayStatusEnum getPayStatusEnum() {
		return EnumUtil.getByCode(payStatus, PayStatusEnum.class);
	}
}
