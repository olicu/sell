package com.josehp.dto;

import lombok.Data;

/**
 * Created by Josehp
 * on 2018/2/19 0019  14:03.
 */
@Data
public class CartDTO {
	/**
	 * 商品id.
	 */
	private String productId;
	
	/**
	 * 商品数量.
	 */
	private Integer productQuantity;
	
	public CartDTO(String productId, Integer productQuantity) {
		this.productId = productId;
		this.productQuantity = productQuantity;
	}
	
}
