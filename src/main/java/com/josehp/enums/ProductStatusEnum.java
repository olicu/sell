package com.josehp.enums;

import lombok.Getter;

/**
 * @ClassName: ProductStatusEnum
 * @Package com.josehp.enums
 * @Description: 商品状态
 * @Author Josehp
 * @CreateDate 2018/02/13 16:34
 * @Version V1.0
 */
@Getter
public enum ProductStatusEnum implements CodeEnum {
	UP(0, "在架"),
	DOWN(1, "下架");
	
	private Integer code;
	
	private String message;
	
	ProductStatusEnum(Integer code, String message) {
		this.message = message;
		this.code = code;
	}
}
