package com.josehp.enums;

import lombok.Getter;

/**
 * @ClassName: OrderStatusEnum
 * @Package com.josehp.enums
 * @Description: 订单状态
 * @Author Josehp
 * @CreateDate 2018/02/14 23:18
 * @Version V1.0
 */
@Getter
public enum OrderStatusEnum implements CodeEnum {
	NEW(0, "新订单"),
	FINISHED(1, "完结"),
	CANCEL(2, "已取消");
	
	private Integer code;
	
	private String message;
	
	OrderStatusEnum(Integer code, String message) {
		this.message = message;
		this.code = code;
	}
	
/*	public static OrderStatusEnum getOrderStatusEnum(Integer code){
		for(OrderStatusEnum orderStatusEnum: OrderStatusEnum.values()){
			if(orderStatusEnum.getCode().equals(code)){
				return orderStatusEnum;
			}
		}
		return null;
	}*/
	
}
