package com.josehp.enums;

import lombok.Getter;

/**
 * @ClassName: PayStatusEnum
 * @Package com.josehp.enums
 * @Description:
 * @Author Josehp
 * @CreateDate 2018/02/14 23:20
 * @Version V1.0
 */
@Getter
public enum PayStatusEnum implements CodeEnum {
	WAIT(0, "未支付"),
	SUCCESS(1, "支付成功");
	
	private Integer code;
	
	private String message;
	
	PayStatusEnum(Integer code, String message) {
		this.message = message;
		this.code = code;
	}
	
/*	public static PayStatusEnum getPayStatusEnum(Integer code) {
		for ( PayStatusEnum payStatusEnum : PayStatusEnum.values() ) {
			if ( payStatusEnum.getCode().equals(code) ) {
				return payStatusEnum;
			}
		}
		return null;
	}*/
}
