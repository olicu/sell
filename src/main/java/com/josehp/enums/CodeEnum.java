package com.josehp.enums;

/**
 * Created by Josehp
 * on 2018/3/3 0003  15:39.
 */
public interface CodeEnum {
	Integer getCode();
}
