package com.josehp.utils;

import com.josehp.VO.ResultVO;

/**
 * @ClassName: ResultVOUtil
 * @Package com.josehp.utils
 * @Description: Result工具类
 * @Author Josehp
 * @CreateDate 2018/02/14 22:37
 * @Version V1.0
 */
public class ResultVOUtil {
	
	public static ResultVO success(Object object) {
		ResultVO resultVO = new ResultVO();
		resultVO.setData(object);
		resultVO.setCode(0);
		resultVO.setMsg("成功");
		return resultVO;
	}
	
	public static ResultVO success() {
		return success(null);
	}
	
	public static ResultVO error(Integer code, String msg) {
		ResultVO resultVO = new ResultVO();
		resultVO.setMsg(msg);
		resultVO.setCode(code);
		return resultVO;
		
	}
}
