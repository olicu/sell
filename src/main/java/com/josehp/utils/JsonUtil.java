package com.josehp.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Josehp
 * on 2018/3/2 0002  17:41.
 */
public class JsonUtil {
	public static String toJson(Object object) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setPrettyPrinting();
		Gson gson = gsonBuilder.create();
		return gson.toJson(object);
	}
}
