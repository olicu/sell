package com.josehp.utils;

import com.josehp.enums.CodeEnum;

/**
 * Created by Josehp
 * on 2018/3/3 0003  15:41.
 */
public class EnumUtil {
	public static <T extends CodeEnum> T getByCode(Integer code, Class <T> enumClass) {
		for ( T each : enumClass.getEnumConstants() ) {
			if ( code.equals(each.getCode()) ) {
				return each;
			}
		}
		return null;
		
	}
}
