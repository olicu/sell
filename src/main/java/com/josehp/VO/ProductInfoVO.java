package com.josehp.VO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName: ProductInfoVO
 * @Package com.josehp.VO
 * @Description: 商品详情
 * @Author Josehp
 * @CreateDate 2018/02/13 23:36
 * @Version V1.0
 */
@Data
public class ProductInfoVO {
	@JsonProperty("id")
	private String productId;
	
	@JsonProperty("name")
	private String productName;
	
	@JsonProperty("price")
	private BigDecimal productPrice;
	
	@JsonProperty("description")
	private String productDescription;
	
	@JsonProperty("icon")
	private String productIcon;
	
	
}
