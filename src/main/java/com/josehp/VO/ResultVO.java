package com.josehp.VO;

import lombok.Data;

/**
 * @ClassName: ResultVO
 * @Package com.josehp.VO
 * @Description: http请求返回的最外层对象
 * @Author Josehp
 * @CreateDate 2018/02/13 17:01
 * @Version V1.0
 */
@Data
public class ResultVO<T> {
	/**
	 * 错误码
	 */
	private Integer code;
	
	/**
	 * 提示信息
	 */
	private String msg;
	/**
	 * 具体内容
	 */
	private T data;
}
