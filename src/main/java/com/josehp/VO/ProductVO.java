package com.josehp.VO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: ProductVO
 * @Package com.josehp.VO
 * @Description: 商品 包含类目
 * @Author Josehp
 * @CreateDate 2018/02/13 23:40
 * @Version V1.0
 */
@Data
public class ProductVO {
	@JsonProperty("name")
	private String categoryName;
	
	@JsonProperty("type")
	private Integer categoryType;
	
	@JsonProperty("foods")
	private List <ProductInfoVO> productInfoVOList;
}
